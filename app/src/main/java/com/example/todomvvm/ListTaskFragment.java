package com.example.todomvvm;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.todomvvm.database.TaskEntry;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ListTaskFragment extends Fragment implements TaskAdapter.ItemClickListener {

   /* private ListTaskViewModel mViewModel;*/
    private static final String TAG = MainActivity.class.getSimpleName();
    // Member variables for the adapter and RecyclerView
    private RecyclerView mRecyclerView;
    private TaskAdapter mAdapter;

    private MainViewModel viewModel;
    List<TaskEntry> mTaskEntry;

    public List<TaskEntry> getTasks() {
        return mTaskEntry;
    }


    public static ListTaskFragment newInstance() {
        return new ListTaskFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.list_task_fragment, container, false);
        mRecyclerView = view.findViewById(R.id.recyclerViewTasks);
        FloatingActionButton fabButton = view.findViewById(R.id.fab);

        /*FloatingActionButton fabButton = view.findViewById(R.id.fab);

        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a new intent to start an AddTaskActivity
                Intent addTaskIntent = new Intent(context, AddEditTaskActivity.class);
                startActivity(addTaskIntent);
            }
        });*/
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a new intent to start an AddTaskActivity
               /* Intent addTaskIntent = new Intent(getActivity(), AddEditTaskActivity.class);
                startActivity(addTaskIntent);*/
                /*TaskAddFragment taskAddFragment = TaskAddFragment.newInstance();
                assert getFragmentManager() != null;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container, taskAddFragment);
                transaction.addToBackStack(null);
                transaction.commit();adde*/
                AddEditTaskFragment addEditTaskFragment = AddEditTaskFragment.newInstance();
                assert getFragmentManager() != null;
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.container1, addEditTaskFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
        FloatingActionButton callButton = view.findViewById(R.id.fabutton);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"));
                startActivity(intent);
            }
        });
        // Set the layout for the RecyclerView to be a linear layout, which measures and
        // positions items within a RecyclerView into a linear list
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Initialize the adapter and attach it to the RecyclerView
        mAdapter = new TaskAdapter(getActivity(), (TaskAdapter.ItemClickListener) this);
        mRecyclerView.setAdapter(mAdapter);

        DividerItemDecoration decoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(decoration);

        /*
         Add a touch helper to the RecyclerView to recognize when a user swipes to delete an item.
         An ItemTouchHelper enables touch behavior (like swipe and move) on each ViewHolder,
         and uses callbacks to signal when a user is performing these actions.
         */
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            // Called when a user swipes left or right on a ViewHolder
            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                // Here is where you'll implement swipe to delete
                int position = viewHolder.getAdapterPosition();
                TaskEntry task = mAdapter.getTasks().get(position);
                viewModel.deleteTask(task);

            }
        }).attachToRecyclerView(mRecyclerView);

        setHasOptionsMenu(true);

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: Use the ViewModel
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        setUpViewModel();
    }

    private void setUpViewModel() {
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        viewModel.getTasks().observe(getViewLifecycleOwner(), new Observer<List<TaskEntry>>() {
            @Override
            public void onChanged(List<TaskEntry> taskEntries) {
                Log.d(TAG, "Updating list of tasks from LiveData in viewModel");
                mAdapter.setTasks(taskEntries);
            }
        });

    }

    @Override
    public void onItemClickListener(int itemId) {
       /* Intent intent = new Intent(getActivity(),AddEditTaskActivity.class);
        intent.putExtra(AddEditTaskActivity.EXTRA_TASK_ID,itemId);
        startActivity(intent);*/
        AddEditTaskFragment addEditTaskFragment = AddEditTaskFragment.newInstance();
        assert getFragmentManager() != null;
      /*  Intent intent = new Intent(getActivity(),AddEditTaskFragment.class);
        intent.putExtra(AddEditTaskFragment.EXTRA_TASK_ID,itemId);
        startActivity(intent);*/
        Bundle bundle = new Bundle();
        bundle.putSerializable(AddEditTaskFragment.EXTRA_TASK_ID, itemId);
        /*List_Activity fragment = new List_Activity();*/
        addEditTaskFragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container1, addEditTaskFragment);
        transaction.addToBackStack(null);

        transaction.commit();

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.setTasks(getTasks());
                Log.i("", "lets check" + mTaskEntry);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        /*return super.onCreateOptionsMenu(menu);*/
    }


}
