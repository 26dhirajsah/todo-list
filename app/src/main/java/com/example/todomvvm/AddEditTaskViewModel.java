package com.example.todomvvm;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.todomvvm.database.AppDatabase;
import com.example.todomvvm.database.Repository;
import com.example.todomvvm.database.TaskEntry;

class AddEditTaskViewModel extends AndroidViewModel {
    LiveData<TaskEntry> task ;
    Repository repository;

    public LiveData<TaskEntry> getTask() {
        return task;
    }



    public AddEditTaskViewModel(Application application, int taskId) {
        super(application);
        AppDatabase database = AppDatabase.getInstance(application);
        repository = new Repository(database);
        if(taskId != -1)
            task = repository.getTaskById(taskId);
    }

   /* *  AddEditTaskViewModel(Application application, int taskId){
        super(application);
        AppDatabase database = AppDatabase.getInstance(application);
        repository = new Repository(database);
        if(taskId != -1)
            task = repository.getTaskById(taskId);
    }*/

    public void insertTask(TaskEntry task){
        repository.insertTask(task);
    }
    public void updateTask(TaskEntry task){
        repository.updateTask(task);
    }

   /* public LiveData<TaskEntry> getTask() {
        return task;
    }

    private LiveData<TaskEntry> task ;
    Repository repository;

    public AddEditTaskViewModel(Application application, int taskId) {
        super(application);
        AppDatabase database = AppDatabase.getInstance(application);
        task =  database.taskDao().loadTaskById(taskId);
    }

    public void insertTask(TaskEntry task){
        repository.insertTask(task);
    }
    public void updateTask(TaskEntry task){
        repository.updateTask(task);
    }
*/
}
