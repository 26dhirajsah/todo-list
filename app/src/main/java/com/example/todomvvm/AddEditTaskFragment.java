package com.example.todomvvm;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.os.Parcelable;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.todomvvm.database.TaskEntry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddEditTaskFragment extends Fragment {

    private static final String DATE_FORMAT = "dd/MM/yyy";
    // Extra for the task ID to be received in the intent
    public static final String EXTRA_TASK_ID = "extraTaskId";
    // Extra for the task ID to be received after rotation
    public static final String INSTANCE_TASK_ID = "instanceTaskId";
    // Constants for priority
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_MEDIUM = 2;
    public static final int PRIORITY_LOW = 3;
    // Constant for default task id to be used when not in update mode
    private static final int DEFAULT_TASK_ID = -1;
    // Constant for logging
    private static final String TAG = AddEditTaskFragment.class.getSimpleName();
    // Fields for views
    static EditText mEditText;
    RadioGroup mRadioGroup;
    Button mButton;
    AddEditTaskViewModel viewModel;
    DatePickerDialog picker;
    EditText eText;

    private int mTaskId = DEFAULT_TASK_ID;

    public static AddEditTaskFragment newInstance() {
        return new AddEditTaskFragment();
    }

    public static final String ARG_DAY_NAME= "day_name";
    public AddEditTaskFragment() {
        // Required empty public constructor

    }


   /* public static Fragment newInstance(int  task){
        Bundle args = new Bundle();
        args.putInt(ARG_DAY_NAME,task);
        AddEditTaskFragment fragment = new AddEditTaskFragment();
        fragment.setArguments(args);
        return fragment;
    }
*/
   /* @Override
    public void onStart() {
        super.onStart();
        Bundle args= getArguments();
        if (args != null){
            int taskId = args.getInt(ARG_DAY_NAME);
            AddEditTaskViewModelFactory factory = new AddEditTaskViewModelFactory(getActivity().getApplication(), taskId);
            viewModel = ViewModelProviders.of(this, factory).get(AddEditTaskViewModel.class);
            viewModel.getTask().observe(getActivity(), new Observer<TaskEntry>() {
                @Override
                public void onChanged(TaskEntry taskEntry) {
                    Log.d(TAG, "Receiving database update from LiveData");
                    viewModel.getTask().removeObserver(this);
                    populateUI(taskEntry);
                }
            });


        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_add_edit_task, container, false);
        initViews(view);

        if (savedInstanceState != null && savedInstanceState.containsKey(INSTANCE_TASK_ID)) {
            mTaskId = savedInstanceState.getInt(INSTANCE_TASK_ID, DEFAULT_TASK_ID);
        }

        ViewPager viewPager = view.findViewById(R.id.viewPager);
       /* PagerAdapter adapter= new PagerAdapter(getFragmentManager());
        viewPager.setAdapter(adapter);*/
        Bundle bundle = getArguments();



        if (bundle != null) {

            if (mTaskId == DEFAULT_TASK_ID) {
                mButton.setText(R.string.update_button);
                mTaskId = bundle.getInt(EXTRA_TASK_ID, DEFAULT_TASK_ID);
                /*int taskId = args.getInt(ARG_DAY_NAME);*/
                Log.i("bundkle: ", ":::::::::" + mTaskId);
                AddEditTaskViewModelFactory factory = new AddEditTaskViewModelFactory(getActivity().getApplication(), mTaskId);
                viewModel = ViewModelProviders.of(this, factory).get(AddEditTaskViewModel.class);
                viewModel.getTask().observe(getViewLifecycleOwner(), new Observer<TaskEntry>() {
                    @Override
                    public void onChanged(TaskEntry taskEntry) {
                        Log.d(TAG, "Receiving database update from LiveData");
                        viewModel.getTask().removeObserver(this);
                        populateUI(taskEntry);
                    }
                });

            }
        } else {
            AddEditTaskViewModelFactory factory = new AddEditTaskViewModelFactory(getActivity().getApplication(), mTaskId);
            viewModel = ViewModelProviders.of(this, factory).get(AddEditTaskViewModel.class);
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(INSTANCE_TASK_ID, mTaskId);
        super.onSaveInstanceState(outState);
    }

    private void initViews(View view) {
        mEditText = view.findViewById(R.id.editTextTaskDescription);
        mRadioGroup = view.findViewById(R.id.radioGroup);


        eText = (EditText) view.findViewById(R.id.editText1);
        eText.setInputType(InputType.TYPE_CLASS_TEXT);
        eText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, day, month, year);
                picker.show();
            }
        });


        mButton = view.findViewById(R.id.saveButton);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSaveButtonClicked();
            }
        });
    }

    private void populateUI(TaskEntry task) {
        if (task == null) {
            return;
        } else {
            mEditText.setText(task.getDescription());
            setPriorityInViews(task.getPriority());
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            String dateTime = dateFormat.format(task.getUpdatedAt());
            eText.setText(dateTime);
        }

    }

    public void onSaveButtonClicked() {
        // Not yet implemented

        String description = mEditText.getText().toString();
        if (description.isEmpty()) {
            AlertDialog.Builder myAlertBuilder = new AlertDialog.Builder(getActivity());

            // Set the dialog title and message.
            myAlertBuilder.setTitle(R.string.alert_title);
            myAlertBuilder.setMessage(R.string.alert_message);

            // Add the dialog buttons.
            myAlertBuilder.setPositiveButton(R.string.ok_button,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // User clicked OK button.
                           /* Toast.makeText(getApplicationContext(),
                                    R.string.pressed_ok,
                                    Toast.LENGTH_SHORT).show();*/
                            return;
                        }
                    });
            // Create and show the AlertDialog.
            myAlertBuilder.show();
        }

        int priority = getPriorityFromViews();
        String dateStr = eText.getText().toString();
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        try {
            Date date = format.parse(dateStr);
            TaskEntry task = new TaskEntry(description, priority, date);
            if (mTaskId == DEFAULT_TASK_ID) {
                Log.d(TAG, "HELLO");
                viewModel.insertTask(task);
                Toast.makeText(getActivity(),
                        R.string.task_inserted,
                        Toast.LENGTH_SHORT).show();
            } else {
                Log.d(TAG, "HELLO from update");
                Log.d(TAG, "HELLO from update"+mTaskId);

                task.setId(mTaskId);
                Log.d(TAG, "HELLO from update"+task);
                viewModel.updateTask(task);
                Toast.makeText(getActivity(),
                        R.string.task_updated,
                        Toast.LENGTH_SHORT).show();
            }

            ListTaskFragment listTaskFragment = ListTaskFragment.newInstance();
            assert getFragmentManager() != null;
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.container1, listTaskFragment);
            transaction.addToBackStack(null);
            transaction.commit();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * getPriority is called whenever the selected priority needs to be retrieved
     */
    public int getPriorityFromViews() {
        int priority = 1;
        int checkedId = ((RadioGroup) getActivity().findViewById(R.id.radioGroup)).getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.radButton1:
                priority = PRIORITY_HIGH;
                break;
            case R.id.radButton2:
                priority = PRIORITY_MEDIUM;
                break;
            case R.id.radButton3:
                priority = PRIORITY_LOW;
        }
        return priority;
    }

    /**
     * setPriority is called when we receive a task from MainActivity
     *
     * @param priority the priority value
     */
    public void setPriorityInViews(int priority) {
        switch (priority) {
            case PRIORITY_HIGH:
                ((RadioGroup) getActivity().findViewById(R.id.radioGroup)).check(R.id.radButton1);
                break;
            case PRIORITY_MEDIUM:
                ((RadioGroup) getActivity().findViewById(R.id.radioGroup)).check(R.id.radButton2);
                break;
            case PRIORITY_LOW:
                ((RadioGroup) getActivity().findViewById(R.id.radioGroup)).check(R.id.radButton3);
        }
    }
}
